#!/bin/sh

echo "Modifiziere .zshrc ..."
curl -Lo ~/.zshrc https://gitlab.com/hopro-makerspace/makerspace/-/raw/master/terminal/.zshrc
source ~/.zshrc

echo "Installiere Python-Packages ..."
pip install tinkerforge

echo " "
echo " "
echo "Fertig! "
