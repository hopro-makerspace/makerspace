#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
HOST = "localhost"
PORT = 4223
humidityBrickletUID = "LjU" # UID aus dem Brick Viewer

# Import der Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_humidity_v2 import BrickletHumidityV2

# Verbindung herstellen
ipcon = IPConnection()
humidityBricklet = BrickletHumidityV2(humidityBrickletUID, ipcon)
ipcon.connect(HOST, PORT)

# Das Hauptprogramm
humidity = humidityBricklet.get_humidity()
print("Luftfeuchtigkeit: " + str(humidity/100.0) + "%")

# Programm wird beendet, wenn eine beliebige Taste gedrückt wird
input("Press key to exit\n")
ipcon.disconnect()
