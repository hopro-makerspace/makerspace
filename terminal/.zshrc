echo '___  ___  ___   _   __ ___________  ___________  ___  _____  _____ '
echo '|  \/  | / _ \ | | / /|  ___| ___ \/  ___| ___ \/ _ \/  __ \|  ___|'
echo '| .  . |/ /_\ \| |/ / | |__ | |_/ /\ `--.| |_/ / /_\ \ /  \/| |__  '
echo '| |\/| ||  _  ||    \ |  __||    /  `--. \  __/|  _  | |    |  __| '
echo '| |  | || | | || |\  \| |___| |\ \ /\__/ / |   | | | | \__/\| |___ '
echo '\_|  |_/\_| |_/\_| \_/\____/\_| \_|\____/\_|   \_| |_/\____/\____/ '
echo ' '
echo ' '

# If you come from bash you might have to change your $PATH.
export PATH=/opt/anaconda3/bin:$PATH

# Set CLICOLOR if you want Ansi Colors in iTerm2
export CLICOLOR=1

# Set colors to match iTerm2 Terminal Colors
export TERM=xterm-256color
