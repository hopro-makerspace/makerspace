#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
# Diese beiden Zeilen nur 1x pro Programm nutzen:
HOST = "localhost"
PORT = 4223

# UID des Bricklets
accelerationBrickletUID = "xxx" # UID aus dem Brick Viewer

# Import der wichtigen Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_accelerometer_v2 import BrickletAccelerometerV2

# Verbindung herstellen
ipcon = IPConnection()
accelerationBricklet = BrickletAccelerometerV2(accelerationBrickletUID, ipcon)
ipcon.connect(HOST, PORT)

# Das Hauptprogramm
x, y, z = accelerationBricklet.get_acceleration()

print("Beschleunigung in x-Richtung: " + str(x/10000) + " g")
print("Beschleunigung in y-Richtung: " + str(y/10000) + " g")
print("Beschleunigung in z-Richtung: " + str(z/10000) + " g")


# Programm wird beendet, wenn eine beliebige Taste gedrückt wird
input("Press key to exit\n")
ipcon.disconnect()