#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
# Diese beiden Zeilen nur 1x pro Programm nutzen:
HOST = "localhost"
PORT = 4223

# UID des Bricklets
soundPressureBrickletUID = "xxx" # UID aus dem Brick Viewer

# Import der wichtigen Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_sound_pressure_level import BrickletSoundPressureLevel

# Verbindung herstellen
ipcon = IPConnection() # Create IP connection
soundPressureBricklet = BrickletHumidityV2(soundPressureBricklet, ipcon)
ipcon.connect(HOST, PORT) # Connect to brickd

# Das Hauptprogramm
decibel = soundPressureBricklet.get_decibel()
print("Decibel: " + str(decibel/10.0) + " dB(A)")

# Programm wird beendet, wenn eine beliebige Taste gedrückt wird
input("Press key to exit\n")
ipcon.disconnect()
