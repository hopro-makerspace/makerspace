#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
# Diese beiden Zeilen nur 1x pro Programm nutzen:
HOST = "localhost"
PORT = 4223
    
# UID des Bricklets
ledstripUID = "xxx" # UID aus dem Brick Viewer

# Import der wichtigen Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_led_strip_v2 import BrickletLEDStripV2

# Verbindung herstellen
ipcon = IPConnection() # Create IP connection
ledstripBricklet = BrickletLEDStripV2(ledstripUID, ipcon) # Create device object
ipcon.connect(HOST, PORT) # Connect to brickd

# LED-Strip-Typ des 30er Streifens
ledstripBricklet.set_chip_type(2812)
# RGB-Eigenschaften setzen
ledstripBricklet.set_channel_mapping(18)
# Auffrischung alle 100 ms
ledstripBricklet.set_frame_duration(100)

# Unsere Strips haben 30 LEDs
numLEDs = 30
# alle 30 LEDs aus
rgb_values = [0, 0, 0]*numLEDs

# Die Callback-Funktion
def ledstripCallback(id):

    # Wir setzen für alle LEDs die gleichen, beliebigen RGB-Werte
    rgb_values[0::3]= [random.randint(1,255)]*numLEDs
    rgb_values[1::3]= [random.randint(1,255)]*numLEDs
    rgb_values[2::3]= [random.randint(1,255)]*numLEDs

    ledstripBricklet.set_led_values(0, rgb_values)


# Das Hauptprogramm

# stellt die LEDs mal aus
ledstripBricklet.set_led_values(0, rgb_values)

# Die Auffrischungsrate ein wenig länger machen
ledstripBricklet.set_frame_duration(1000)
ledstripBricklet.register_callback(ledstripBricklet.CALLBACK_FRAME_STARTED, ledstripCallback)

# Programm wird beendet, wenn eine beliebige Taste gedrückt wird
input("Press key to exit\n") # Use raw_input() in Python 2
ipcon.disconnect()
