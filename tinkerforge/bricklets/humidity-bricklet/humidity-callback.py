#!/usr/bin/env python
# -*- coding: utf-8 -*-

HOST = "localhost"
PORT = 4223
humidityBrickletUID = "LkJ" # Change XYZ to the UID of your Temperature Bricklet 2.0

from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_humidity_v2 import BrickletHumidityV2


ipcon = IPConnection() # Create IP connection
humidityBricklet = BrickletHumidityV2(humidityBrickletUID, ipcon) # Create device object
ipcon.connect(HOST, PORT) # Connect to brickd

def humidityCallback(humidity):

    print("Luftfeuchtigkeit: " + str(humidity/100.0) + " %")


if __name__ == "__main__":

    # Die Callback-Funktion registrieren
    humidityBricklet.register_callback(humidityBricklet.CALLBACK_HUMIDITY, humidityCallback)

    # Die Callback-Funktion soll ausgeführt werden, falls die Werte nicht
    # innerhlab von 30 - 60 % liegen
    humidityBricklet.set_humidity_callback_configuration(10000, False, "o", 30*100, 60*100)

    input("Press key to exit\n")
    ipcon.disconnect()
