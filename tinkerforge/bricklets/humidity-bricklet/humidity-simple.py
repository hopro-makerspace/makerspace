#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
# Diese beiden Zeilen nur 1x pro Programm nutzen:
HOST = "localhost"
PORT = 4223

# UID des Bricklets
humidityBrickletUID = "LkJ" # Change XYZ to the UID of your Temperature Bricklet 2.0

# Import der wichtigen Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_humidity_v2 import BrickletHumidityV2

# Verbindung herstellen
ipcon = IPConnection() # Create IP connection
humidityBricklet = BrickletHumidityV2(humidityBrickletUID, ipcon) # Create device object
ipcon.connect(HOST, PORT) # Connect to brickd

# Das Hauptprogramm
humidity = humidityBricklet.get_humidity()
print("Luftfeuchtigkeit: " + str(humidity/100.0) + " %")

# Programm wird beendet, wenn eine beliebige Taste gedrückt wird
input("Press key to exit\n")
ipcon.disconnect()
