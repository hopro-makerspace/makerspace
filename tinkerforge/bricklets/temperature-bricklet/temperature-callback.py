#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
# Diese beiden Zeilen nur 1x pro Programm nutzen:
HOST = "localhost"
PORT = 4223

# UID des Bricklets 
temperatureBrickletUID = "xxx" # UID aus dem Brick Viewer

# Import der wichtigen Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_temperature_v2 import BrickletTemperatureV2

# Verbindung herstellen
ipcon = IPConnection()
tempBricklet = BrickletTemperatureV2(temperatureBrickletUID, ipcon)
ipcon.connect(HOST, PORT)

# Callback function for temperature callback
def tempCallback(temperature):
    print("Temperature: " + str(temperature/100.0) + " °C")

# Das Haptprogramm

# Register temperature callback to function cb_temperature
tempBricklet.register_callback(t.CALLBACK_TEMPERATURE, tempCallback)

# Set period for temperature callback to 1s (1000ms) without a threshold
tempBricklet.set_temperature_callback_configuration(1000, False, "x", 0, 0)

# Programm wird beendet, wenn eine beliebige Taste gedrückt wird
input("Press key to exit\n")
ipcon.disconnect()
