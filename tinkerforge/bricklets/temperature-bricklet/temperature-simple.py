#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
# Diese beiden Zeilen nur 1x pro Programm nutzen:
HOST = "localhost"
PORT = 4223

# UID des Bricklets 
temperatureBrickletUID = "xxx" # UID aus dem Brick Viewer

# Import der wichtigen Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_temperature_v2 import BrickletTemperatureV2

# Verbindung herstellen
ipcon = IPConnection()
tempBricklet = BrickletTemperatureV2(temperatureBrickletUID, ipcon)
ipcon.connect(HOST, PORT)

# Das Hauptprogramm
temperature = tempBricklet.get_temperature()
print("Temperature: " + str(temperature/100.0) + " °C")

# Programm wird beendet, wenn eine beliebige Taste gedrückt wird
input("Press key to exit\n")
ipcon.disconnect()
