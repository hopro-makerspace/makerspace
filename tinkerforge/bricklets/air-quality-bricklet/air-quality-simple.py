#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
# Diese beiden Zeilen nur 1x pro Programm nutzen:
HOST = "localhost"
PORT = 4223

# UID des Bricklets
airqualityBrickletUID = "xxx" # UID aus dem Brick Viewer

# Import der wichtigen Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_air_quality import BrickletAirQuality

# Verbindung herstellen
ipcon = IPConnection()
airqualityBricklet = BrickletAirQuality(airqualityBrickletUID, ipcon)
ipcon.connect(HOST, PORT)

# Das Hauptprogramm
iaq, accuracy, temp, hum, pressure = airqualityBricklet.get_all_values()

print("Air Quality Index: " + str(iaq))
print("Genauigkeit: " + str(accuracy))
print("Temoperatur: " + str(temp/100) + " °C")
print("Luftfeuchtigkeit: " + str(hum/100) + " %")
print("Luftdruck: " + str(pressure/100) + " mbar")


# Programm wird beendet, wenn eine beliebige Taste gedrückt wird
input("Press key to exit\n")
ipcon.disconnect()

