#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
# Diese beiden Zeilen nur 1x pro Programm nutzen:
HOST = "localhost"
PORT = 4223

# UID des Bricklets
joystickBrickletUID = "xxx" # UID aus dem Brick Viewer

# Import der wichtigen Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_joystick_v2 import BrickletJoystickV2

# Verbindung herstellen
ipcon = IPConnection()
joystickBricklet = BrickletJoystickV2(joystickBrickletUID, ipcon)
ipcon.connect(HOST, PORT)

# Die Funktion welche aktiviert wird, wenn der Joystick gedrückt wird
def joystickCallbackPressed(pressed):
    print("Pressed: " + str(pressed))
    print("")

# Die Funktion welche aktiviert wird, wenn der Joystick bewegt wird
def joystickCallbackPosition(x, y):
    print("Position X: " + str(x))
    print("Position Y: " + str(y))
    print("")


# Das Hauptprogramm

# Callbackfunktionen für Drücker registrieren
joystickBricklet.register_callback(joystickBricklet.CALLBACK_PRESSED, joystickCallbackPressed)

# Callbackfunktionen für Position registrieren
joystickBricklet.register_callback(joystickBricklet.CALLBACK_POSITION, joystickCallbackPosition)

# Callbackfunktion für Drücker konfigurieren
joystickBricklet.set_pressed_callback_configuration(10, True)

# Callbackfunktion für Position konfigurieren
joystickBricklet.set_position_callback_configuration(100, True)

# Programm wird beendet, wenn eine beliebige Taste gedrückt wird
input("Press key to exit\n")
ipcon.disconnect()

