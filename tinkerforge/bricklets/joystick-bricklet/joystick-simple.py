#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
# Diese beiden Zeilen nur 1x pro Programm nutzen:
HOST = "localhost"
PORT = 4223

# UID des Bricklets
joystickBrickletUID = "MAV" # UID aus dem Brick Viewer

# Import der wichtigen Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_joystick_v2 import BrickletJoystickV2

# Verbindung herstellen
ipcon = IPConnection()
joystickBricklet = BrickletJoystickV2(joystickBrickletUID, ipcon)
ipcon.connect(HOST, PORT)

# Das Hauptprogramm
x, y = joystickBricklet.get_position()
pressed = joystickBricklet.is_pressed()

print("Position X: " + str(x))
print("Position Y: " + str(y))
print("Pressed: " + str(pressed))


# Programm wird beendet, wenn eine beliebige Taste gedrückt wird
input("Press key to exit\n")
ipcon.disconnect()