#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
# Diese beiden Zeilen nur 1x pro Programm nutzen:
HOST = "localhost"
PORT = 4223
    
# UID des Bricklets
rgbledBrickletUID = "xxx" # UID aus dem Brick Viewer

# Import der wichtigen Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_rgb_led_v2 import BrickletRGBLEDV2

# Verbindung herstellen
ipcon = IPConnection()
rgbledBricklet = BrickletAirQuality(rgbledBrickletUID, ipcon)
ipcon.connect(HOST, PORT)


# Das Hauptprogramm
rgbledBricklet.set_rgb_value(0, 170, 234)


# Programm wird beendet, wenn eine beliebige Taste gedrückt wird
input("Press key to exit\n")
ipcon.disconnect()

