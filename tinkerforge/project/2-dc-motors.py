#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
# Diese beiden Zeilen nur 1x pro Programm nutzen:
HOST = "localhost"
PORT = 4223

# UID der Brick 
dc1BrickUID = "xxxxxx" # UID aus dem Brick Viewer (6-stellig)
dc2BrickUID = "xxxxxx" # UID aus dem Brick Viewer (6-stellig)

# Import der wichtigen Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.brick_dc import BrickDC

# Die Bibliothek wird fürs Hauptpogramm gebraucht
import time

# Verbindung herstellen
ipcon = IPConnection()
dc1Brick = BrickDC(dc1BrickUID, ipcon)
dc2Brick = BrickDC(dc2BrickUID, ipcon)

ipcon.connect(HOST, PORT)

# DC-Motor-Einstellungen konfigurieren. Das sind Standardeinstellungen
dc1Brick.set_drive_mode(dc1Brick.DRIVE_MODE_DRIVE_BRAKE)
dc2Brick.set_drive_mode(dc2Brick.DRIVE_MODE_DRIVE_BRAKE)
dc1Brick.set_pwm_frequency(10000)
dc2Brick.set_pwm_frequency(10000)
dc1Brick.set_acceleration(55000)
dc2Brick.set_acceleration(55000)

# Das Hauptprogramm

# Volle Gescwindigkeit (100 %)
dc1Brick.set_velocity(32767)
dc2Brick.set_velocity(32767)
dc1Brick.enable() # Enable motor power
dc2Brick.enable() # Enable motor power


# 5 Sekunden fahren, danach mit kleinerer Geschwindigkeit weiter
time.sleep(5)
dc1Brick.set_velocity(20000)
dc2Brick.set_velocity(20000)



# Nach 2 s stoppen
time.sleep(2)
dc1Brick.set_velocity(0)
dc2Brick.set_velocity(0)


# 2 s warten, bis Motor gestoppt hat, danach abschalten.
time.sleep(2)
dc1Brick.disable()
dc2Brick.disable()

ipcon.disconnect()
