#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
# Diese beiden Zeilen nur 1x pro Programm nutzen:
HOST = "localhost"
PORT = 4223

# UID der Brick 
dcBrickUID = "xxxxxx" # UID aus dem Brick Viewer (6-stellig)

# Import der wichtigen Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.brick_dc import BrickDC

# Die Bibliothek wird fürs Hauptpogramm gebraucht
import time

# Verbindung herstellen
ipcon = IPConnection()
dcBrick = BrickDC(dcBrickUID, ipcon)
ipcon.connect(HOST, PORT)

# DC-Motor-Einstellungen konfigurieren. Das sind Standardeinstellungen
dcBrick.set_drive_mode(dcBrick.DRIVE_MODE_DRIVE_BRAKE)
dcBrick.set_pwm_frequency(10000)
dcBrick.set_acceleration(55000)

# Das Hauptprogramm

# Volle Gescwindigkeit (100 %)
dcBrick.set_velocity(32767)
dcBrick.enable() # Enable motor power

# 5 Sekunden fahren, danach mit kleinerer Geschwindigkeit weiter
time.sleep(5)
dcBrick.set_velocity(20000)


# Nach 2 s stoppen
time.sleep(2)
dcBrick.set_velocity(0)

# 2 s warten, bis Motor gestoppt hat, danach abschalten.
time.sleep(2)
dcBrick.disable()

ipcon.disconnect()
