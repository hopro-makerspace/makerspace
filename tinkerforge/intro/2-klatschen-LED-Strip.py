#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
HOST = "localhost"
PORT = 4223
ledstripBrickletUID = "XYZ" # Gib hier die UID deines LED-Strip-Bricklets ein
soundPressureBrickletUID = "XYZ" # Change XYZ to the UID of your Temperature Bricklet 2.0

# Import der Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_sound_pressure_level import BrickletSoundPressureLevel
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_led_strip_v2 import BrickletLEDStripV2


# Verbindung herstellen
ipcon = IPConnection()
ledstripBricklet = BrickletLEDStripV2(ledstripBrickletUID, ipcon)
soundPressureBricklet = BrickletSoundPressureLevel(soundPressureBrickletUID, ipcon)
ipcon.connect(HOST, PORT)

# LED-Strip-Typ des 30er Streifens
ledstripBricklet.set_chip_type(2812)
# RGB-Eigenschaften setzen
ledstripBricklet.set_channel_mapping(18)
# Auffrischung alle 100 ms
ledstripBricklet.set_frame_duration(100)

# Unsere Strips haben 30 LEDs
num_leds = 30
# alle 30 LEDs aus
rgb_values = [0, 0, 0]*num_leds

# Diese Variable brauchen wir um den Zustan des LED zu speichern (An oder Aus)
istAn = False


# Die Callback-Funktion, die ausgeführt wird, wenn 80dB überschritten werden
def soundCallback(decibel):

    # die Variable istAn global machen, damit Änderungen hier nicht verloren gehen
    global istAn

    # Falls LED an, dann ausschalten oder umgekehrt
    if istAn:
        rgb_values = [0, 0, 0]*num_leds
        istAn = False
    else:
        rgb_values = [0, 0, 255]*num_leds
        istAn = True

    ledstripBricklet.set_led_values(0, rgb_values)


# Das Hauptpgrogramm
# stellt die LEDs mal aus
ledstripBricklet.set_led_values(0, rgb_values)

# Die Callback-Funktion für das Sound-Pressure-Bricklet registrieren
soundPressureBricklet.register_callback(soundPressureBricklet.CALLBACK_DECIBEL, soundCallback)

# Callback aktivieren, falls 80 dB überschritten werden
soundPressureBricklet.set_decibel_callback_configuration(1000, False, ">", 800, 0)

# Programm wird beendet, wenn eine beliebige Taste gedrückt wird
input("Press key to exit\n")
ipcon.disconnect()
