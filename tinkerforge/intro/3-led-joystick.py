#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
# Diese beiden Zeilen nur 1x pro Programm nutzen:
HOST = "localhost"
PORT = 4223
    
# UID des Bricklets
ledstripUID = "xxx" # UID aus dem Brick Viewer
joystickBrickletUID = "xxx" # UID aus dem Brick Viewer

# Import der wichtigen Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_led_strip_v2 import BrickletLEDStripV2
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_joystick_v2 import BrickletJoystickV2

# Verbindung herstellen
ipcon = IPConnection()
ledstripBricklet = BrickletLEDStripV2(ledstripUID, ipcon)
joystickBricklet = BrickletJoystickV2(joystickBrickletUID, ipcon)
ipcon.connect(HOST, PORT)

# LED-Strip-Typ des 30er Streifens
ledstripBricklet.set_chip_type(2812)
# RGB-Eigenschaften setzen
ledstripBricklet.set_channel_mapping(18)
# Auffrischung alle 100 ms
ledstripBricklet.set_frame_duration(100)

# Unsere Strips haben 30 LEDs
numLEDs = 30
# alle 30 LEDs aus
rgb_values = [0, 0, 0]*numLEDs

positionLED = 0
maxsteps = 18
step = 0

# schaltet alle LEDs aus
ledstripBricklet.set_led_values(0, rgb_values)

# schaltet 1. LED ein
ledstripBricklet.set_led_values(0, [255, 0, 0])

# Diese Funktion gibt die Regenbogenfarben in Schritten zurück.
# steps ist die Farbe die man wünscht
# maxsteps ist die maximale Unterteilung des Regenbogens
def getRainbowColor(step, maxsteps):
    number = step/maxsteps*6

    if number <= 1:
        return [255, int(step/maxsteps*6*255), 0]
    elif number <= 2:
        return [int((1-(step/maxsteps*6-1))*255), 255, 0]
    elif number <= 3:
        return [0, 255, int((step/maxsteps*6-2)*255)]
    elif number <= 4:
        return [0, int((1-(step/maxsteps*6-3))*255), 255]
    elif number <= 5:
        return [int((step/maxsteps*6-4)*255), 0, 255]
    elif number <= 6:
        return [255, 0, int((1-(step/maxsteps*6-5))*255)]


# Die Funktion welche aktiviert wird, wenn der Joystick bewegt wird
def joystickCallbackPosition(x, y):

    # Da wir positionLED und step in dieser Funktion verändern möchten
    # müssen wir diese auf global setzen
    global positionLED, step

    # Falls Joystick nach oben oder unten gedrückt wird, soll sich die Farbe ändern
    # Die Schwelle ab +60 oder -60 machen den Joystick nicht zu sensitiv
    if y >= 60:
        step = (step + 1) % maxsteps
    
    if y <= -60:
        step = (step - 1) % maxsteps

    # Die momentane mit steps und getRainbowcolor verändern 
    color = getRainbowColor(step, maxsteps)
    ledstripBricklet.set_led_values(3*positionLED, [0,0,0])
    ledstripBricklet.set_led_values(3*positionLED, color)
    
    # Falls Joystick nach oben oder unten gedrückt wird, soll sich die LED-Position ändern
    # Die Schwelle ab +60 oder -60 machen den Joystick nicht zu sensitiv
    if x >= 60:
        if positionLED < 29:
            ledstripBricklet.set_led_values(3*positionLED, [0,0,0])
            positionLED = positionLED + 1
            ledstripBricklet.set_led_values(3*positionLED, color)

    elif x <= -60:
        if positionLED > 0:
            ledstripBricklet.set_led_values(3*positionLED, [0,0,0])
            positionLED = positionLED - 1
            ledstripBricklet.set_led_values(3*positionLED, color)



        


# Das Hauptprogramm

# Callbackfunktionen für Position registrieren
joystickBricklet.register_callback(joystickBricklet.CALLBACK_POSITION, joystickCallbackPosition)

# Callbackfunktion für Position konfigurieren
# Die Persiode von 200 ms muss grösser als die Refresh-Periode der LED sein!
joystickBricklet.set_position_callback_configuration(200, False)

# Programm wird beendet, wenn eine beliebige Taste gedrückt wird
input("Press key to exit\n") # Use raw_input() in Python 2
ipcon.disconnect()
