#!/usr/bin/env python
# -*- coding: utf-8 -*-

HOST = "localhost"
PORT = 4223
UID = "Lbb" # Gib hier die UID deines LED-Strip-Bricklets ein

# Hier laden wir die Tinkerforge-Module
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_led_strip_v2 import BrickletLEDStripV2

# das brauchen wir um Random-Farben zu erzeugen
import random

# und stellen eine Verbindung zum LED-Strip-Bricklet her
ipcon = IPConnection() # Create IP connection
ledstripBricklet = BrickletLEDStripV2(UID, ipcon) # Create device object
ipcon.connect(HOST, PORT) # Connect to brickd
ledstripBricklet.set_chip_type(2812)
ledstripBricklet.set_channel_mapping(18)
ledstripBricklet.set_frame_duration(100)
num_leds = 30
# alle 30 LEDs aus
rgb_values = [0, 0, 0]*num_leds

blueLED = 0
motion = 1

def ledstripCallback(id):
    # mit global erreichen wir, dass Änderungen an den 3 Variabeln auch
    # ausserhalb der Funktion gespeichert werden
    global blueLED, motion, rgb_values

    # Das aktuelle blaue LED ausschalten
    rgb_values[2+3*blueLED] = 0

    if motion == 1:
        if blueLED < 29:
            blueLED = blueLED + 1
        else:
            motion = -1
            blueLED = blueLED - 1
    else:
        if blueLED > 0:
            blueLED = blueLED - 1
        else:
            motion = 1
            blueLED = blueLED + 1

    rgb_values[2+3*blueLED] = 255
    ledstripBricklet.set_led_values(0, rgb_values)


if __name__ == "__main__":
    # stellt die LEDs mal aus
    ledstripBricklet.set_led_values(0, rgb_values)

    # Die Auffrischungsrate ein wenig länger machen
    ledstripBricklet.set_frame_duration(100)
    ledstripBricklet.register_callback(ledstripBricklet.CALLBACK_FRAME_STARTED, ledstripCallback)

    input("Press key to exit\n") # Use raw_input() in Python 2
    ipcon.disconnect()
