#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
HOST = "localhost"
PORT = 4223
rgbledBrickletUID = "xyz" # Gib hier die UID deines LED-Strip-Bricklets ein
soundPressureBrickletUID = "xyz" # Change XYZ to the UID of your Temperature Bricklet 2.0

# Import der Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_sound_pressure_level import BrickletSoundPressureLevel
from tinkerforge.bricklet_rgb_led_v2 import BrickletRGBLEDV2


# Verbindung herstellen
ipcon = IPConnection()
rgbledBricklet = BrickletRGBLEDV2(rgbledBrickletUID, ipcon)
soundPressureBricklet = BrickletSoundPressureLevel(soundPressureBrickletUID, ipcon)
ipcon.connect(HOST, PORT)

# Diese Variable brauchen wir um den Zustan des LED zu speichern (An oder Aus)
istAn = False


# Die Callback-Funktion, die ausgeführt wird, wenn 80dB überschritten werden
def soundCallback(decibel):

    # die Variable istAn global machen, damit Änderungen hier nicht verloren gehen
    global istAn

    # Falls LED an, dann ausschalten oder umgekehrt
    if istAn:
        rgbledBricklet.set_rgb_value(0, 0, 0)
        istAn = False
    else:
        rgbledBricklet.set_rgb_value(0, 0, 255)
        istAn = True


# Das Hauptprogramm

# stellt die LED mal aus
rgbledBricklet.set_rgb_value(0, 0, 0)

# Die Callback-Funktion für das Sound-Pressure-Bricklet registrieren
soundPressureBricklet.register_callback(soundPressureBricklet.CALLBACK_DECIBEL, soundCallback)

# Callback aktivieren, falls 80 dB überschritten werden, evtl muss ein höherer Wert genommen werden
soundPressureBricklet.set_decibel_callback_configuration(1000, False, ">", 800, 0)

# Programm wird beendet, wenn eine beliebige Taste gedrückt wird
input("Press key to exit\n")
ipcon.disconnect()
