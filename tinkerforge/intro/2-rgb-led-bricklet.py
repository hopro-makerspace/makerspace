#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
HOST = "localhost"
PORT = 4223
rgbledBrickletUID = "xxx" # UID aus dem Brick Viewer

# Import der Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_rgb_led_v2 import BrickletRGBLEDV2

# Verbindung herstellen
ipcon = IPConnection()
rgbledBricklet = BrickletRGBLEDV2(rgbledBrickletUID, ipcon)
ipcon.connect(HOST, PORT)

# Das Hauptprogramm

# Stell die LED auf eine Farbe (0,170,234), hier türkis, ein
rgbledBricklet.set_rgb_value(0, 170, 234)

# Programm wird beendet, wenn eine beliebige Taste gedrückt wird
input("Press key to exit\n")
ipcon.disconnect()