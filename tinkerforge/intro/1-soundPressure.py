#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adressierung
HOST = "localhost"
PORT = 4223
soundPressureBrickletUID = "XYZ" # UID aus dem Brick Viewer

# Import der Funktionen
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_sound_pressure_level import BrickletSoundPressureLevel

# Verbindung herstellen
ipcon = IPConnection()
soundPressureBricklet = BrickletSoundPressureLevel(soundPressureBrickletUID, ipcon) # Create device object
ipcon.connect(HOST, PORT)

# Das Hauptprogramm
decibel = soundPressureBricklet.get_decibel()
print("Decibel: " + str(decibel/10.0) + " dB(A)")

# Programm wird beendet, wenn eine beliebige Taste gedrückt wird
input("Press key to exit\n")
ipcon.disconnect()
